/**
 * Module dependencies.
 */

var express = require('express')
  , http = require('http')
  , path = require('path')
  , swig = require('swig')
  , session = require('express-session')
  , MongoStore = require('connect-mongo')(session);

var app = express();

var appTitle = 'Eurovision 2015';

/*
  important note right here! do not fuck up!
*/

app.configure(function() {
  app.set('port', process.env.PORT || 3002);
  app.set('views', __dirname + '/views');
  app.engine('html', swig.renderFile);
  app.set('view engine', 'html');
  app.use(express.logger('dev'));
  app.use(express.methodOverride());
  app.use(express.json());
  app.use(express.urlencoded());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function() {
  app.use(express.errorHandler());
});

/*
  Base request
 */
app.get('/', function(req, res) {
  res.render('index', {
    title: appTitle,
    user: {
      name: ''
    }
  });
});

app.get('/vote', function(req, res) {

  var contestants = [
    {
      'id':11,
      'order': 1,
      'code': 'fi',
      'country': 'Finland',
      'song': {
        'artist': 'Steamengine',
        'name': 'Something Better'
      }
    },
    {
      'id':12,
      'order': 2,
      'code': 'ru',
      'country': 'Russia',
      'song': {
        'artist': 'Artisti',
        'name': 'Kappale'
      }
    },
    {
      'id':13,
      'order': 3,
      'code': 'es',
      'country': 'Spain',
      'song': {
        'artist': 'Artisti',
        'name': 'Kappale'
      }
    },
    {
      'id':14,
      'order': 4,
      'code': 'ee',
      'country': 'Estonia',
      'song': {
        'artist': 'Artisti',
        'name': 'Kappale'
      }
    },
    {
      'id':15,
      'order': 5,
      'code': 'pl',
      'country': 'Poland',
      'song': {
        'artist': 'Artisti',
        'name': 'Kappale'
      }
    },
    {
      'id':16,
      'order': 6,
      'code': 'be',
      'country': 'Belgium',
      'song': {
        'artist': 'Artisti',
        'name': 'Kappale'
      }
    }
  ]

  res.render('vote', {
    title: appTitle,
    'contestants': contestants
  });
});

app.get('/results', function(req, res) {
  res.render('results', {
    title: appTitle
  });
});

app.post('/user/create', function(req, res) {
  res.json(200, {'params': req.body});
});

app.post('/user/login', function(req, res) {
  res.redirect('/');
});

app.get('/user/login/:id', function(req, res) {
  res.json(200, {'id': req.params.id});
});

app.post('/user/logout', function(req, res) {
  res.redirect('/');
});

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
