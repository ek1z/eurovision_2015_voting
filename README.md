# README #

Eurovision voting application. Enables users to vote and monitor given votes during or after the contest.

### Planned funtionality ###

* User account creation without password for easy access.
* Session data / local storage to save given votes or user data.
* Socket.io for realtime results page 
